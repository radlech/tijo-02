import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

interface ListOfTeamToLineConverter {

    /** Wykonuje konwersje listy obiektow typu Team do listy linii wygenerowanych w odpowiednim formacie */
    List<String> toLines(List<Team> teams, String connectionBy, String header);
}

interface CsvReport {

    /** funkcja tworzy plik CSV oraz zapisuje go w okreslonej lokalizacji w systemie operacyjnym
     *
     * @param lines linie w odpowiednim formacie do zapisu
     * @return <b>true</b> w przypadku pomyslnego zapisu pliku, w przeciwnym wypadku zwraca <b>false</b>
     * */
    boolean saveCsvReport(List<String> lines);
}

/** klasa generuje liste druzyn z wloskiej seria A */
class GenerateTeamStatistics {

    /** pobierz druzyny w wloskiej serie A */
    public static List<Team> getTeams() {

        List<Team> listOfTeams = new ArrayList<>();

        listOfTeams.add(new Team(1, "SSC Napoli", 66, 25, 21, 3, 1, "55-15"));
        listOfTeams.add(new Team(2, "Juventus Turyn", 65, 25, 21, 2, 2, "62-15"));
        listOfTeams.add(new Team(3, "AS Roma", 50, 25, 15, 5, 5, "40-19"));
        listOfTeams.add(new Team(4, "Lazio Rzym", 49, 25, 15, 4, 6, "60-33"));
        listOfTeams.add(new Team(5, "Inter Mediolan", 48, 25, 13, 9, 3, "40-21"));
        listOfTeams.add(new Team(6, "Sampdoria Genoa", 41, 25, 12, 5, 8, "44-33"));
        listOfTeams.add(new Team(7, "AC Milan", 41, 25, 12, 5, 8, "35-30"));
        listOfTeams.add(new Team(8, "Atalanta Bergamo", 38, 25, 10, 8, 7, "37-29"));
        listOfTeams.add(new Team(9, "Torino", 36, 25, 8, 12, 5, "35-30"));
        listOfTeams.add(new Team(10, "Udinese Calcio", 33, 25, 10, 3, 12, "36-35"));
        listOfTeams.add(new Team(11, "ACF Fiorentina", 32, 25, 8, 8, 9, "34-32"));
        listOfTeams.add(new Team(12, "Genoa", 30, 25, 8, 6, 11, "21-25"));
        listOfTeams.add(new Team(13, "Bologna FC", 30, 25, 9, 3, 13, "31-38"));
        listOfTeams.add(new Team(14, "Cagliari", 25, 25, 7, 4, 14, "23-36"));
        listOfTeams.add(new Team(15, "Chievo Werona", 25, 25, 6, 7, 12, "23-42"));
        listOfTeams.add(new Team(16, "US Sassuolo Calcio", 23, 25, 6, 5, 14, "15-43"));
        listOfTeams.add(new Team(17, "FC Crotone", 21, 25, 5, 6, 14, "21-44"));
        listOfTeams.add(new Team(18, "SPAL", 17, 25, 3, 8, 14, "23-47"));
        listOfTeams.add(new Team(19, "Hellas Verona", 16, 25, 4, 4, 17, "22-50"));
        listOfTeams.add(new Team(20, "Benevento Calcio", 10, 25, 3, 1, 21, "18-58"));

        return listOfTeams;
    }
}

class Team {

    private int id, points, games, wins, draws, loses;
    private String team, goals;

    Team(int id, String team, int points, int games, int wins, int draws, int loses, String goals) {
        this.id = id;
        this.team = team;
        this.points = points;
        this.games = games;
        this.wins = wins;
        this.draws = draws;
        this.loses = loses;
        this.goals = goals;
    }

    public int getId() {
        return id;
    }

    public int getPoints() {
        return points;
    }

    public int getGames() {
        return games;
    }

    public int getWins() {
        return wins;
    }

    public int getDraws() {
        return draws;
    }

    public int getLoses() {
        return loses;
    }

    public String getTeam() {
        return team;
    }

    public String getGoals() {
        return goals;
    }

    private int getBalance() {
        String[] balance = goals.split("-");
        return Integer.parseInt(balance[0]) - Integer.parseInt(balance[1]);
    }

    public String toString(String separator) {
        return id + separator + team + separator + games + separator + points + separator + wins + separator +
                draws + separator + loses + separator + goals + separator + getBalance();
    }
}

class SaveCsvFile implements CsvReport {

    String file;

    SaveCsvFile(String file) {
        this.file = file;
    }

    @Override
    /** funkcja tworzy plik CSV oraz zapisuje go w okreslonej lokalizacji w systemie operacyjnym
     *
     * @param lines linie w odpowiednim formacie do zapisu
     * @return <b>true</b> w przypadku pomyslnego zapisu pliku, w przeciwnym wypadku zwraca <b>false</b>
     * */
    public boolean saveCsvReport(List<String> lines) {

        try {

            PrintWriter save = new PrintWriter(file);

            for(String line : lines) {
                save.println(line);
            }

            save.close();
            return true;

        } catch(FileNotFoundException e) {
            return false;
        }
    }
}

class ListOfTeamToLineConverterImpl implements ListOfTeamToLineConverter {

    @Override
    /** Wykonuje konwersje listy obiektow typu Team do listy linii wygenerowanych w odpowiednim formacie */
    public List<String> toLines(List<Team> teams, String connectionBy, String header) {

        List<String> listOfTeams = new ArrayList<>();

        listOfTeams.add(header);

        for (Team team : teams) {
            listOfTeams.add(team.toString(connectionBy));
        }

        return listOfTeams;
    }
}

class Main {

    public static void main(String[] args) {

        final String HEADER = "LP;DRUZYNA;MECZE;PUNKTY;ZWYCIESTWA;REMISY;PORAZKI;BRAMKI;ROZNICA;";
        final String CONNECTION_BY = ";";
        final String CSV_FILE = "D:\\serieA.csv";

        List<Team> teams = GenerateTeamStatistics.getTeams();
        ListOfTeamToLineConverter listOfTeamToLineConverter = new ListOfTeamToLineConverterImpl();
        List<String> linesOfTeams = listOfTeamToLineConverter.toLines(teams, CONNECTION_BY, HEADER);

        CsvReport csvReport = new SaveCsvFile(CSV_FILE);

        boolean isSaved = csvReport.saveCsvReport(linesOfTeams);

        if(isSaved) {
            System.out.println("Plik " + CSV_FILE + " zostal zapisany");
        } else {
            System.out.println("Blad podczas zapisu pliku!");
        }
    }
}